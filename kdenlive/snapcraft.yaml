---
name: kdenlive
confinement: strict
grade: stable
base: core20
adopt-info: kdenlive
apps:
    kdenlive:
        extensions:
        - kde-neon
        common-id: org.kde.kdenlive.desktop
        command: usr/bin/kdenlive
        plugs:
        - home
        - network
        - network-bind
        - audio-playback
        - audio-record
        - removable-media
        - system-observe
    pip:
        command: bin/pip
        plugs:
        - network
        - home
environment:
    PYTHONPYCACHEPREFIX: "$SNAP_USER_COMMON/.pycache"
    PYTHONUSERBASE: "$SNAP_USER_COMMON/.local"
    PIP_USER: 1
    PYTHONPATH: "$PYTHONUSERBASE/lib/python3.8/site-packages:$SNAP/lib/python3.8/site-packages:$SNAP/usr/lib/python3/dist-packages"
    MLT_REPOSITORY: "$SNAP/usr/lib/$SNAPCRAFT_ARCH_TRIPLET/mlt-7/"
    MLT_DATA: "$SNAP/usr/share/mlt-7/"
    MLT_ROOT_DIR: "$SNAP/usr/"
    LADSPA_PATH: "$SNAP/usr/lib/ladspa"
    FREI0R_PATH: "$SNAP/usr/lib/frei0r-1"
    MLT_PROFILES_PATH: "$SNAP/usr/share/mlt-7/profiles/"
    MLT_PRESETS_PATH: "$SNAP/usr/share/mlt-7/presets/"
slots:
    session-dbus-interface:
        interface: dbus
        name: org.kde.kdenlive
        bus: session
package-repositories:
-   type: apt
    components:
    - main
    suites:
    - focal
    key-id: 444DABCF3667D0283F894EDDE6D4736255751E5D
    url: http://origin.archive.neon.kde.org/user
    key-server: keyserver.ubuntu.com
parts:
    python-deps:
        plugin: python
        python-packages:
        - vosk
        - srt
        - pip
        stage:
        - "-pyvenv.cfg"
    glaxnimate:
        plugin: cmake
        source: https://gitlab.com/mattbas/glaxnimate.git
        source-tag: 0.5.1
        build-packages:
        - qt5-image-formats-plugins
        - qttools5-dev
        - libpython3-dev
        - zlib1g-dev
        - libpotrace-dev
        - libarchive-dev
        - libavformat-dev
        - libswscale-dev
        - libavcodec-dev
        - libavutil-dev
        cmake-parameters:
        - "-DCMAKE_INSTALL_PREFIX=/usr"
        - "-DCMAKE_BUILD_TYPE=Release"
        - "-DCMAKE_FIND_ROOT_PATH=/usr\\;$SNAPCRAFT_STAGE\\;/snap/kde-frameworks-5-98-qt-5-15-6-core20-sdk/current"
        - "-DBUILD_TESTING=OFF"
    kdenlive:
        after:
        - python-deps
        - glaxnimate
        parse-info:
        - usr/share/metainfo/org.kde.kdenlive.appdata.xml
        plugin: cmake
        build-packages:
        - libkf5doctools-dev
        - libkf5purpose-dev
        - libmlt++-dev
        - librttr-dev
        - libmlt-dev
        - libv4l-dev
        stage-packages:
        -   on amd64:
            - libpsm-infinipath1
        - ffmpeg
        - frei0r-plugins
        - librttr-core0.9.6
        - libkf5purpose-bin
        - melt
        - dvgrab
        - recordmydesktop
        - swh-plugins
        - libslang2
        - libmagic1
        - libmlt++7
        - libgpm2
        - kio-extras
        - mediainfo
        source: http://download.kde.org/stable/release-service/22.08.2/src/kdenlive-22.08.2.tar.xz
        cmake-parameters:
        - "-DCMAKE_INSTALL_PREFIX=/usr"
        - "-DCMAKE_BUILD_TYPE=Release"
        - "-DMOD_GLAXNIMATE=ON"
        - "-DCMAKE_FIND_ROOT_PATH=/usr\\;$SNAPCRAFT_STAGE\\;/snap/kde-frameworks-5-98-qt-5-15-6-core20-sdk/current"
        - "-DBUILD_TESTING=OFF"
        override-build: |
            snapcraftctl build
            sed -i "${SNAPCRAFT_PART_INSTALL}/usr/share/applications/org.kde.kdenlive.desktop" -e 's|Icon=kdenlive$|Icon=${SNAP}/usr/share/icons/hicolor/scalable/apps/kdenlive.svg|'
            cd "${SNAPCRAFT_PART_INSTALL}/usr/share/icons/hicolor/scalable/apps"
            gzip -d -c kdenlive.svgz > kdenlive.svg
            if [ "$SNAPCRAFT_TARGET_ARCH" = amd64 ]; then
                ln -sf ../libpsm1/libpsm_infinipath.so.1.16  $SNAPCRAFT_PART_INSTALL/usr/lib/$SNAPCRAFT_ARCH_TRIPLET/libpsm_infinipath.so.1
            fi
    cleanup:
        after:
        - kdenlive
        - python-deps
        - glaxnimate
        plugin: nil
        build-snaps:
        - kde-frameworks-5-98-qt-5-15-6-core20
        override-prime: |
            set -eux
            for snap in "kde-frameworks-5-98-qt-5-15-6-core20"; do  # List all content-snaps you're using here
                cd "/snap/$snap/current" && find . -type f,l -exec rm -f "$SNAPCRAFT_PRIME/{}" "$SNAPCRAFT_PRIME/usr/{}" \;
            done
            for CRUFT in bug lintian man; do
                rm -rf $SNAPCRAFT_PRIME/usr/share/$CRUFT
            done
            find $SNAPCRAFT_PRIME/usr/share/doc/ -type f -not -name 'copyright' -delete
            find $SNAPCRAFT_PRIME/usr/share -type d -empty -delete
version: 22.08.2
