---
name: kdevelop
confinement: classic
grade: stable
base: core20
adopt-info: kdevelop
compression: lzo
apps:
    kdevelop:
        command: usr/bin/kdevelop
        common-id: org.kde.kdevelop.desktop
        environment:
            KDEV_CLANG_BUILTIN_DIR: "$SNAP/usr/lib/llvm-11/lib/clang/11.0.0/include"
            QTWEBENGINEPROCESS_PATH: "$SNAP/usr/lib/$SNAPCRAFT_ARCH_TRIPLET/qt5/libexec/QtWebEngineProcess"
            XDG_DATA_DIRS: "$XDG_DATA_DIRS:$SNAP/usr/share"
            XDG_CONFIG_DIRS: "$XDG_CONFIG_DIRS:$SNAP/etc/xdg"
            __EGL_VENDOR_LIBRARY_DIRS: "$SNAP/etc/glvnd/egl_vendor.d:$SNAP/usr/share/glvnd/egl_vendor.d"
            LIBGL_DRIVERS_PATH: "$SNAP/usr/lib/${SNAPCRAFT_ARCH_TRIPLET}/dri"
            KDEV_UNSET_ENV: LIBGL_DRIVERS_PATH
package-repositories:
-   type: apt
    components:
    - main
    suites:
    - focal
    key-id: 444DABCF3667D0283F894EDDE6D4736255751E5D
    url: http://origin.archive.neon.kde.org/user
    key-server: keyserver.ubuntu.com
parts:
    mesa-patchelf:
        plugin: nil
        stage-packages:
        - libgl1-mesa-dri
        - libglu1-mesa
        stage:
        - "-usr/lib/${SNAPCRAFT_ARCH_TRIPLET}/dri"
    mesa-no-patchelf:
        plugin: nil
        stage-packages:
        - libgl1-mesa-dri
        build-attributes:
        - no-patchelf
        stage:
        - usr/lib/${SNAPCRAFT_ARCH_TRIPLET}/dri
    qtconf:
        plugin: nil
        override-build: |
            mkdir -p $SNAPCRAFT_PART_INSTALL/usr/bin
            cat <<EOF > $SNAPCRAFT_PART_INSTALL/usr/bin/qt.conf
            [Paths]
            Prefix = ../../
            LibraryExecutables = usr/lib/$SNAPCRAFT_ARCH_TRIPLET/qt5/libexec
            Plugins = usr/lib/$SNAPCRAFT_ARCH_TRIPLET/qt5/plugins
            Qml2Imports = usr/lib/$SNAPCRAFT_ARCH_TRIPLET/qt5/qml
            Translations = usr/share/qt5/translations
            Data = usr/share/qt5
            EOF
    patches:
        plugin: dump
        source: patches/
        prime:
        - "-*"
    kdevelop:
        after:
        - patches
        plugin: cmake
        build-packages:
        - extra-cmake-modules
        - libboost-dev
        - libastyle-dev
        - libkf5doctools-dev
        - libclang-11-dev
        - libkomparediff2-dev
        - libsvn-dev
        - okteta-dev
        - libkf5purpose-dev
        - libkf5sysguard-dev
        - kdevelop-pg-qt
        - libgrantlee5-dev
        - libkf5config-dev
        - libkf5crash-dev
        - libkf5declarative-dev
        - libkf5i18n-dev
        - libkf5iconthemes-dev
        - libkf5itemmodels-dev
        - libkf5itemviews-dev
        - libkf5jobwidgets-dev
        - libkf5kcmutils-dev
        - libkf5kio-dev
        - libkf5newstuff-dev
        - libkf5notifyconfig-dev
        - libkf5parts-dev
        - libkf5plasma-dev
        - libkf5runner-dev
        - libkf5service-dev
        - libkf5syntaxhighlighting-dev
        - libkf5texteditor-dev
        - libkf5threadweaver-dev
        - libkf5windowsystem-dev
        - libkf5xmlgui-dev
        - libqt5webkit5-dev
        - gettext
        - patch
        stage-packages:
        - konsole-kpart
        - libgrantlee-templates5
        - libgrantlee-textdocument5
        - libapr1
        - libaprutil1
        - libastyle3
        - libclang1-11
        - libclang-11-dev
        - libkasten4controllers0
        - libkasten4core0
        - libkasten4gui0
        - libkasten4okteta2controllers0
        - libkasten4okteta2core0
        - libkasten4okteta2gui0
        - libkomparediff2-5
        - libokteta3core0
        - libokteta3gui0
        - libqca-qt5-2
        - libserf-1-1
        - libsvn1
        - libutf8proc2
        - llvm-11-dev
        - libkf5purpose5
        - libksysguardformatter1
        - libprocesscore9
        - libprocessui9
        - libbrotli1
        - libeditorconfig0
        - libgstreamer-plugins-base1.0-0
        - libgstreamer1.0-0
        - libhyphen0
        - libkf5itemmodels5
        - libkf5plasma5
        - libkf5runner5
        - libkf5syntaxhighlighting5
        - libkf5texteditor5
        - libkf5threadweaver5
        - liborc-0.4-0
        - libqt5sensors5
        - libqt5sql5
        - libqt5webkit5
        - libwoff1
        - freeglut3
        - libqt5gui5
        - libqt5svg5
        - qtwayland5
        - qt5-image-formats-plugins
        - qml-module-qtquick2
        - qml-module-qtquick-controls
        - qml-module-qtquick-layouts
        - qml-module-qtquick-window2
        - qml-module-qtquick-xmllistmodel
        - qml-module-qtwebkit
        - qml-module-qtquick-controls2
        - breeze
        parse-info:
        - usr/share/metainfo/org.kde.kdevelop.appdata.xml
        source: http://download.kde.org/stable/release-service/22.08.2/src/kdevelop-22.08.2.tar.xz
        cmake-parameters:
        - "-DCMAKE_INSTALL_PREFIX=/usr"
        - "-DCMAKE_BUILD_TYPE=Release"
        - "-DENABLE_TESTING=OFF"
        - "-DBUILD_TESTING=OFF"
        - "-DKDE_SKIP_TEST_SETTINGS=ON"
        - "-DCMAKE_FIND_ROOT_PATH=/usr\\;$SNAPCRAFT_STAGE"
        override-build: |
            snapcraftctl build
            sed -i "${SNAPCRAFT_PART_INSTALL}/usr/share/applications/org.kde.kdevelop.desktop" \
                -e 's|Icon=kdevelop$|Icon=${SNAP}/usr/share/icons/hicolor/scalable/apps/kdevelop.svg|'
        override-pull: |
            snapcraftctl pull
            patch -p1 < $SNAPCRAFT_STAGE/kdevelop_unset_env.patch
version: 22.08.2
